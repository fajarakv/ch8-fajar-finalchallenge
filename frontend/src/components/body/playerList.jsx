import React from "react";
import { Card, Badge, Button } from "react-bootstrap";
import Swal from "sweetalert2";

const PlayerList = ({
  username,
  email,
  experience,
  level,
  onDelete,
  onUpdate,
}) => {
  return (
    <Card className="border-0 shadow">
      <Card.Body>
        <Card.Title>
          {username} <Badge bg="secondary">{level}</Badge>
        </Card.Title>
        <Card.Subtitle>{email}</Card.Subtitle>
        <Card.Text>{experience}</Card.Text>
      </Card.Body>
      <Card.Footer className="text-center">
        <Button variant="success" size="sm" onClick={() => onUpdate(username)}>
          Edit
        </Button>{" "}
        <Button 
          variant="danger" 
          size="sm" 
          onClick={() => {
            Swal.fire({
              title: "Are You Sure",
              text: "You Will Delete This Data",
              icon: "warning",
              showCancelButton: true,
              confirmButtonColor: "#3085d6",
              cancelButtonColor: "#d33",
              confirmButtonText: "Yes",
              cancelButtonText: "Cancel"
            }).then((result) => {
              if (result.isConfirmed) {
                Swal.fire(
                  "Deleted",
                  "Player has been deleted",
                  "success"
                )
                onDelete(username)
              }
            })
          }}
        >
          Delete
        </Button>
      </Card.Footer>
    </Card>
  );
};

export default PlayerList;
