import React from "react";
import { Form, Button, Row, Col } from "react-bootstrap";
import Swal from "sweetalert2";

const PlayerInput = ({ onSubmit, data, update }) => {
  const [username, setUsername] = React.useState("");
  const [email, setEmail] = React.useState("");
  const [experience, setExperience] = React.useState("");
  const [level, setLevel] = React.useState("0");

  React.useEffect(() => {
    setUsername(data.username);
    setEmail(data.email);
    setExperience(data.experience);
    setLevel(data.level);
  }, [data]);

  return (
    <Form>
      <Row className="mb-3">
        <Form.Label className="text-center fs-3 mb-3 fw-bold">
          {update === true ? "Edit Player Data" : "Add Player Data"}
        </Form.Label>
        <Form.Group as={Col} controlId="formGridUsername">
          <Form.Label>Username</Form.Label>
          <Form.Control
            type="text"
            placeholder="Username"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
        </Form.Group>
        <Form.Group as={Col} controlId="formGridEmail">
          <Form.Label>Email</Form.Label>
          <Form.Control
            type="email"
            placeholder="example@e.com"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </Form.Group>
      </Row>
      <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
        <Form.Label>Experience</Form.Label>
        <Form.Control
          as="textarea"
          rows={3}
          placeholder="Fill Your Experience Here"
          value={experience}
          onChange={(e) => setExperience(e.target.value)}
        />
      </Form.Group>
      <Form.Group className="mb-3">
        <Form.Label>Level</Form.Label>
        <Form.Range value={level} onChange={(e) => setLevel(e.target.value)} />
        <Form.Control type="number" value={level} onChange={(e) => setLevel(e.target.value)}/>
      </Form.Group>
      
        <Button
         disabled={!username+ !email+ !experience+ !level}
          variant="primary"
          onClick={() => {
            const Toast = Swal.mixin({
              toast: true,
              position: 'top-end',
              showConfirmButton: false,
              timer: 2000,
              timerProgressBar: true,
              didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
              }
            })
            
            Toast.fire({
              icon: 'success',
              title: update === true ? 'Success Updating Player' : 'Success Adding Player'
            })
            onSubmit({ username, email, experience, level });
            setUsername("");
            setEmail("");
            setExperience("");
            setLevel("0");
          }}
        >
          {update === true ? "Update" : "Submit"}
        </Button>
      
    </Form>
  );
};

export default PlayerInput;
