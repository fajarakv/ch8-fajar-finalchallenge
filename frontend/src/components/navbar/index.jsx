import React from "react";
import {Navbar,Container,Form,SplitButton,Dropdown} from "react-bootstrap";
import Icon from "../../assets/logo.png";

const navigationBar = ({basedOn, setBasedOn, keyword, setKeyword, onSearch}) => {
  return (
    <Navbar bg="dark" variant="dark" className="shadow">
      <Container fluid>
        <Navbar.Brand href="#" className="title m-3">
          <img src={Icon} width="60" height="60" className="mx-3"/>
          {""} Ajay Pages
        </Navbar.Brand>
        <Form className="d-flex mx-3">
            <Form.Control
              type="search"
              placeholder={`Search By ${basedOn}`}
              className="me-2"
              aria-label="Search"
              value={keyword}
              onChange={(e) => setKeyword(e.target.value)}
            />
            <SplitButton
              variant="success"
              title={basedOn}
              id="input-group-dropdown-2"
              align="end"
              onClick={() => onSearch()}
            >
              <Dropdown.Item onClick={() => setBasedOn("Username")}>
                Username
              </Dropdown.Item>
              <Dropdown.Item onClick={() => setBasedOn("Email")}>Email</Dropdown.Item>
              <Dropdown.Item onClick={() => setBasedOn("Exp")}>
                Experience
              </Dropdown.Item>
              <Dropdown.Item onClick={() => setBasedOn("Level")}>Level</Dropdown.Item>
            </SplitButton>
          </Form>
      </Container>
    </Navbar>
  );
};

export default navigationBar;