import React, { useState } from "react";
import { Card, Col, Container, Row, Alert, Button } from "react-bootstrap";
import Navbar from "./components/navbar";
import PlayerInput from "./components/body/playerInput";
import PlayerList from "./components/body/playerList";

function App() {
  const [players, setPlayers] = useState([]);
  const [error, setError] = useState("");
  const [playerUp, setPlayerUp] = useState({});
  const [update, setUpdate] = useState(false);
  const [playersFilter, setPlayersFilter] = useState([]);
  const [basedOn, setBasedOn] = useState("Username");
  const [keyword, setKeyword] = useState("");

  const submitHandler = (player) => {
    if (update === true) {
      const newPlayers = players.map((i) => i.username === playerUp.username ? { ...player } : i);
      setPlayers(newPlayers);
      setPlayerUp({});
      setUpdate(false)
    } else {
      const isFind = players.find((i) => i.username === player.username || i.email === player.email)
      if (!isFind) {
        setPlayers([...players, player]);
      } else {
        setError("Email or Username Existed");
        setTimeout(() => {
          setError("")
        }, 2000)
      }
    }
  }

  const deleteHandler = (params) => {
    const newPlayers = players.filter((i) => i.username !== params);
    setPlayers(newPlayers);
  }

  const updateHandler = (params) => {
    const isFind = players.find((i) => i.username === params);
    setPlayerUp(isFind);
    setUpdate(true);
  }

  const searchHandler = () => {
    const filtered = players.filter((i) => {
      switch (basedOn) {
        case "Username":
          return i.username.toLowerCase().includes(
            keyword.toLowerCase()
          );
        case "Email":
          return i.email.toLowerCase().includes(
            keyword.toLowerCase()
          );
        case "Exp":
          return i.pengalaman.toLowerCase().includes(
            keyword.toLowerCase()
          );
        case "Level":
          return i.level.toLowerCase().includes(
            keyword.toLowerCase()
          )
        default:
          return basedOn
      }
    })

    if (filtered.length === 0) {
      setError(`There is no player named "${keyword}" based on "${basedOn}"`)
      setTimeout(() => {
        setError("")
      }, 3000)
    }

    setPlayersFilter(filtered)
    setKeyword("")
    setBasedOn("Username")
  }


  return (
    <>
    <Navbar
      basedOn={basedOn}
      setBasedOn={setBasedOn}
      keyword={keyword}
      setKeyword={setKeyword}
      onSearch={searchHandler}
    />
    <Container className="d-grid gap-4 mt-3">
        <Row className="d-flex justify-content-center">
          <Card className="p-4 mt-4 w-50 border-0 shadow">
            <PlayerInput onSubmit={submitHandler} data={playerUp} update={update} />
          </Card>
        </Row>
        <Row className="d-flex justify-content-center">
          {error && (
            <Alert variant="danger" className="w-50">
              {error}
            </Alert>
          )}
        </Row>
        
        <Row className="d-grid justify-content-center">
          <Button variant="outline-secondary" onClick={() => setPlayersFilter([])}>All Players</Button>{" "}
        </Row>
        <Row lg={4}>
          {playersFilter.length > 0
            ? playersFilter.map((data, i) => {
              return (
                <Col className="mb-4 d-grid">
                  <PlayerList
                    {...data}
                    key={i}
                    onDelete={deleteHandler}
                    onUpdate={updateHandler}
                  />
                </Col>
              )
            })
            : players.map((data, i) => {
              return (
                <Col className="mb-4 d-grid">
                  <PlayerList
                    {...data}
                    key={i}
                    onDelete={deleteHandler}
                    onUpdate={updateHandler}
                  />
                </Col>
              )
            })}
          {players.length === 0 && (
            <Container className="w-50 fs-5 fw-bold text-center">
              Empty Players Data
            </Container>
          )}
        </Row>
      </Container>
    </>
  );
}

export default App;
